FROM alpine:latest

RUN apk update && apk add --no-cache python3 jq curl bash git && \
    pip3 install --upgrade pip setuptools httpie && \
    rm -r /root/.cache

ADD Dockerfile Dockerfile
ADD deployment_message_to_slack.sh deployment_message_to_slack.sh